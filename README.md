# CLS Smooth Path Generation

Generates a high-resolution smooth path from a set of points using constrained least squares (CLS) optimization.